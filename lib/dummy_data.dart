
// ignore_for_file: non_constant_identifier_names

import 'package:flutter/material.dart';

import './models/umkm.dart';

List<Umkm> DUMMY_UMKM = [
  Umkm(
    id: 0,
    merekBisnis: 'Bisnis 1',
    domisili: 'Aceh',
    produkJasa: 'Perabot',
    pendanaanDibutuhkan: 10000000,
    sahamUmkm: 500000,
    deskripsi: 'Toko bagus',
    logoUsaha: 'test',
    gambarUsaha: 'test',
    ringkasanPerusahaan: 'test',
  ),
  Umkm(
    id: 1,
    merekBisnis: 'Bisnis 2',
    domisili: 'Aceh',
    produkJasa: 'Perabot Juga',
    pendanaanDibutuhkan: 15000000,
    sahamUmkm: 500200,
    deskripsi: 'Toko agak bagus',
    logoUsaha: 'test',
    gambarUsaha: 'test',
    ringkasanPerusahaan: 'test',
  ),
  Umkm(
    id: 3,
    merekBisnis: 'Bisnis 3',
    domisili: 'Jawa',
    produkJasa: 'Alat Masak',
    pendanaanDibutuhkan: 15000000,
    sahamUmkm: 340000,
    deskripsi: 'Mantap Gaming',
    logoUsaha: 'test',
    gambarUsaha: 'test',
    ringkasanPerusahaan: 'test',
  ),
  Umkm(
    id: 4,
    merekBisnis: 'Bisnis 4',
    domisili: 'Sulawesi',
    produkJasa: 'Alat Makan',
    pendanaanDibutuhkan: 35000000,
    sahamUmkm: 12300000,
    deskripsi: 'Toko sangat bagus',
    logoUsaha: 'test',
    gambarUsaha: 'test',
    ringkasanPerusahaan: 'test',
  ),
];
